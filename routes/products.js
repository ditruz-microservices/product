const express = require('express');
const router = express.Router();

const model = require('../models/index');

const connectRabbitMQ = require('../config/rabbitmq');

/* localhost:4000/api/v1/products/create */
router.post('/create', async function(req, res, next) {
  const { name, price } = req.body;

  //บันทึกลงตาราง products
  const newProduct = await model.Product.create({
    name: name,
    price: price
  });

  //ส่งสินค้าใหม่ผ่าน event bus ไปยัง order-report service
  const channel = await connectRabbitMQ();
  await channel.assertExchange('ex.akenarin', 'direct', { durable: true });
  await channel.assertQueue('q.akenarin.product', { durable: true });
  await channel.bindQueue('q.akenarin.product', 'ex.akenarin', 'rk.akenarin.product');
  channel.publish('ex.akenarin', 'rk.akenarin.product', Buffer.from( JSON.stringify(newProduct)),{
    persistent: true,
    contentType: 'application/json',
    contentEncoding: 'utf-8',
    type: 'ProductCreated'
  });

  return res.status(201).json({
    message: 'เพิ่มข้อมูลสินค้าสำเร็จ',
    product: newProduct
  });

});

module.exports = router;
